@echo off

set CONTAINER=kwetter-post-service

CALL docker build -t "%CONTAINER%:test" .
CALL docker build -t java-docker --target test .

CALL docker-compose -f docker-compose.test.yml -p Kwetter-test up --detach
CALL sleep 60
CALL docker run --rm --network kwetter-test_kwetter --name springboot-test java-docker

CALL docker-compose -f docker-compose.test.yml -p Kwetter-test down
CALL docker system prune -f -a