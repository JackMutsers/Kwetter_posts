package com.kwetter.posting_service.interfaces;

import com.kwetter.posting_service.objects.data_transfer_objects.WriterDTO;

import java.util.List;

public interface IUserService {
    WriterDTO returnUser(String user_id);
    List<WriterDTO> returnUsers(List<String> user_ids);
}
