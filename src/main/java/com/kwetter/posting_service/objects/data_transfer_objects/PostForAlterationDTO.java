package com.kwetter.posting_service.objects.data_transfer_objects;

import com.kwetter.posting_service.helpers.tools.Helper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.kwetter.posting_service.helpers.tools.Helper.IsEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PostForAlterationDTO {
    private int id = 0;
    private boolean visible = true;
    private int group_id = 0;
    private boolean group = false;
    private String creation_date = "";
    private String message;

    public PostForAlterationDTO(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public boolean validateForUpdate(){ return ( this.id == 0 || validateForCreation()) || IsEmpty(message); } // true == invalid data, false == correct data

    public boolean validateForCreation(){ return ( this.group_id == 0 || message == null || IsEmpty(message) ); } // true == invalid data, false == correct data

    public void setId(int id) {
        this.id = id;
    }

    public void setCreation_date(LocalDateTime creationDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.creation_date = creationDate.format(formatter);
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public LocalDateTime getCreation_date(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        if(IsEmpty(creation_date)){
            creation_date = LocalDateTime.now().format(formatter);
        }

        return LocalDateTime.parse(creation_date, formatter);
    }
}
