package com.kwetter.posting_service.objects.data_transfer_objects;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class WriterDTO {
    private String id;
    private String displayName;

    public WriterDTO(String id) {
        this.id = id;
    }
}
