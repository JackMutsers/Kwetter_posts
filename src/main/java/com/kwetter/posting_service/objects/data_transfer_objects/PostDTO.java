package com.kwetter.posting_service.objects.data_transfer_objects;

import com.kwetter.posting_service.objects.models.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class PostDTO {
    private int id;
    private int group_Id;
    private String message;
    private String creation_date;
    private String writer;
    private List<CommentDTO> comments;

    public PostDTO(Post post) {
        this.id = post.getId();
        this.group_Id = post.getGroupId();
        this.creation_date = post.getCreationDate();
        this.message = post.getMessage();
    }

    public PostDTO(){}

    public LocalDateTime getCreation_date(){
        creation_date = creation_date.replace("T", " ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(creation_date, formatter);
    }

}
