package com.kwetter.posting_service.objects.data_transfer_objects;

import com.kwetter.posting_service.objects.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RabbitDTO {
    private Object content;
    private MessageType type;
}
