package com.kwetter.posting_service.objects.enums;

public enum MessageType {
    SINGLE,
    LIST
}
