package com.kwetter.posting_service.objects.models;

import com.kwetter.posting_service.objects.data_transfer_objects.CommentForAlterationDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SuppressWarnings("WeakerAccess")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 36)
    private String writer;

    @Column(name = "post_id")
    private int postId;

    @Column(nullable = true)
    private int comment_id;

    @Type(type="text")
    private String message;
    private LocalDateTime creationDate;

    public String getCreationDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return creationDate.format(formatter);
    }

    public Comment(CommentForAlterationDTO DTO, String writer) {
        this.id = DTO.getId();
        this.writer = writer;
        this.postId = DTO.getPost_id();
        this.comment_id = DTO.getComment_id();
        this.message = DTO.getMessage();
        this.creationDate = DTO.getCreation_date();
    }
}


