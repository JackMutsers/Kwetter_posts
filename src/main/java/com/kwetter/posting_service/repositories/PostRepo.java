package com.kwetter.posting_service.repositories;

import com.kwetter.posting_service.objects.models.Post;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PostRepo extends CrudRepository<Post, Integer>{
    Post findById(int id);
    List<Post> findAllByWriterOrderByCreationDateDesc(String user_Id);
    List<Post> findAllByGroupIdOrderByCreationDateDesc(int group_Id);
    void deleteById(int id);
    void deleteAllByWriter(String writer);

    @Transactional
    @Modifying
    @Query(value = "ALTER TABLE post AUTO_INCREMENT = 1;", nativeQuery = true)
    void resetAutoIncrement();
}

