package com.kwetter.posting_service.controllers;

import com.kwetter.posting_service.interfaces.ICommentService;
import com.kwetter.posting_service.interfaces.IPostService;
import com.kwetter.posting_service.interfaces.IUserService;
import com.kwetter.posting_service.objects.data_transfer_objects.CommentDTO;
import com.kwetter.posting_service.objects.data_transfer_objects.PostDTO;
import com.kwetter.posting_service.objects.data_transfer_objects.PostForAlterationDTO;
import com.kwetter.posting_service.objects.data_transfer_objects.WriterDTO;
import com.kwetter.posting_service.objects.exceptions.UnauthorizedException;
import com.kwetter.posting_service.objects.models.Comment;
import com.kwetter.posting_service.objects.models.Post;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.kwetter.posting_service.helpers.tools.Helper.emptyIfNull;

@Controller
@RequestMapping("/post")
public class PostController {
    private final IPostService postService;
    private final ICommentService commentService;
    private final IUserService userService;

    public PostController(IPostService postService, ICommentService commentService, IUserService userService) {
        this.postService = postService;
        this.commentService = commentService;
        this.userService = userService;
    }

    @GetMapping(path = "/get/{post_id}")
    public @ResponseBody ResponseEntity<Object> getPost(HttpServletRequest request, @PathVariable int post_id){
        Post post = null;

        try{
            post = postService.getPost(post_id);
        }catch (NotFoundException ex){
            System.out.println(ex.getMessage());
        }

        if (post == null){
            return new ResponseEntity<>("Requested post could not be found.", HttpStatus.BAD_REQUEST);
        }

        PostDTO postDTO = getPostDTO(post, true);
        return new ResponseEntity<>(postDTO, HttpStatus.OK);
    }

    @GetMapping(path = "/user_posts/{user_id}")
    public @ResponseBody ResponseEntity<Object> getPostsFromUser(HttpServletRequest request, @PathVariable String user_id){
        List<Post> posts = postService.getPostsByUser(user_id);

        WriterDTO writer = userService.returnUser(user_id);

        if(writer == null){
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<PostDTO> dtoList = getPostDTOList(posts, writer);


        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }

    @GetMapping(path = "/group_posts/{group_id}")
    public @ResponseBody ResponseEntity<Object> getPostsFromGroup(HttpServletRequest request, @PathVariable int group_id){
        List<Post> posts = postService.getPostsByGroup(group_id);

        WriterDTO writer = null; //groupService.returnGroup(group_id);

        List<PostDTO> dtoList = getPostDTOList(posts, writer);

        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{post_id}")
    public @ResponseBody ResponseEntity<String> deletePost(HttpServletRequest request, @PathVariable int post_id) {
        String user_id = request.getHeader("x-auth-user-id");
        boolean success = postService.deletePost(post_id, user_id);

        if (!success) {
            return new ResponseEntity<>("something went wrong while deleting the post.", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Post has been deleted successfully.", HttpStatus.OK);
    }

    @PostMapping(path="/create")
    public @ResponseBody ResponseEntity<Object> createPost(HttpServletRequest request, @RequestBody PostForAlterationDTO alterationDTO) {
        String user_id = request.getHeader("x-auth-user-id");
        String origin = request.getHeader("origin");
        System.out.println(origin);

        System.out.println("Create post: " + alterationDTO);

        if(!alterationDTO.validateForCreation()){
            return new ResponseEntity<>("Post could not be created with the supplied information", HttpStatus.CONFLICT);
        }

        alterationDTO.setId(0); // set id to 0 to avoid any unwanted updates
        Post post = postService.createPost(alterationDTO, user_id);

        if (post == null){
            return new ResponseEntity<>("something went wrong wile creating a new Post", HttpStatus.CONFLICT);
        }

        PostDTO postDTO = getPostDTO(post, false);

        return new ResponseEntity<>(postDTO, HttpStatus.CREATED);
    }

    @PutMapping(path ="/update")
    public @ResponseBody ResponseEntity<Object> updatePost(HttpServletRequest request, @RequestBody PostForAlterationDTO alterationDTO) {
        String user_id = request.getHeader("x-auth-user-id");
        if(!alterationDTO.validateForUpdate()){
            return new ResponseEntity<>("Post could not be updated with the supplied information.", HttpStatus.CONFLICT);
        }

        try {
            Post post = postService.updatePost(alterationDTO, user_id);

            if (post == null){
                return new ResponseEntity<>("Something went wrong while updating the post", HttpStatus.CONFLICT);
            }

            PostDTO postDTO = getPostDTO(post,true);

            return new ResponseEntity<>(postDTO, HttpStatus.OK);
        }catch (NotFoundException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }catch (UnauthorizedException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.UNAUTHORIZED);
        }catch (Exception ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public PostDTO getPostDTO(Post post, Boolean withComments){
        PostDTO postDTO = new PostDTO(post);


        WriterDTO writer = userService.returnUser(post.getWriter());

        if(writer == null){
            System.out.println("No writer could be found with id: " + post.getWriter());
        }

        postDTO.setWriter(writer.getDisplayName());

        if(withComments){
            List<Comment> comments = commentService.getComments(post.getId());
            List<WriterDTO> writers = commentService.getWriters(comments);

            List<CommentDTO> commentDTOS = getCommentDTOList(comments, writers);
            postDTO.setComments(commentDTOS);
        }

        return postDTO;
    }

    public List<PostDTO> getPostDTOList(List<Post> posts, WriterDTO writer){
        List<PostDTO> dtoList = new ArrayList<>();
        for (Post post : emptyIfNull(posts))
        {
            PostDTO postDTO = this.getPostDTO(post, false);
            postDTO.setWriter(writer.getDisplayName());
            dtoList.add(postDTO);
        }

        return dtoList;
    }

    public List<CommentDTO> getCommentDTOList(List<Comment> posts, List<WriterDTO> writers){
        List<CommentDTO> dtoList = new ArrayList<>();
        for (Comment comment : emptyIfNull(posts))
        {
            CommentDTO commentDTO = new CommentDTO(comment);

            WriterDTO writerDTO = writers.stream().filter(
                    writer -> writer.getId().equals(comment.getWriter())).findFirst().orElse(new WriterDTO(comment.getWriter())
            );
            commentDTO.setWriter(writerDTO.getDisplayName());

            dtoList.add(commentDTO);
        }

        return dtoList;
    }
}
