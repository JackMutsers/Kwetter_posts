package com.kwetter.posting_service.services;

import com.kwetter.posting_service.helpers.logger.LoggerService;
import com.kwetter.posting_service.interfaces.IPostService;
import com.kwetter.posting_service.objects.data_transfer_objects.PostForAlterationDTO;
import com.kwetter.posting_service.objects.exceptions.UnauthorizedException;
import com.kwetter.posting_service.objects.models.Post;
import com.kwetter.posting_service.repositories.PostRepo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.kwetter.posting_service.helpers.tools.Helper.emptyIfNull;

@Service
public class PostService implements IPostService {

    private boolean debug = false;
    private PostRepo _postRepo;

    public PostService(PostRepo postRepo){
        _postRepo = postRepo;
    }

    public Post getPost(int id) throws NotFoundException {
        Post post = null;

        try {
            post = _postRepo.findById(id);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        if(post == null && !debug){
            throw new NotFoundException("The requested post with id: " + id + " could not be found");
        }

        return post;
    }

    public List<Post> getPostsByUser(String user_id) {
        List<Post> posts = _postRepo.findAllByWriterOrderByCreationDateDesc(user_id);
        return posts;
    }

    public List<Post> getPostsByGroup(int group_id) {
        List<Post> posts = _postRepo.findAllByGroupIdOrderByCreationDateDesc(group_id);
        return posts;
    }

    public boolean deletePost(int id, String user) {
        try{
            Post post = getPost(id);

            if(post == null){
                System.out.println("requested post with id: " + id + " could not be found for user: " + user);
                return true;
            }

            if(!post.getWriter().equals(user)){
                throw new UnsupportedOperationException("user with id: " + user + " does not have permission to delete this post");
            }

            _postRepo.delete(post);
            return true;
        }catch (Exception ex){
            LoggerService.warn(ex.getMessage());
            return false;
        }
    }

    public Post createPost(PostForAlterationDTO alterationDTO, String user) {
        Post post = new Post(alterationDTO, user);
        post.setId(0);
        Post newObject = _postRepo.save(post);
        return newObject;
    }

    public Post updatePost(PostForAlterationDTO alterationDTO, String user) throws UnauthorizedException, NotFoundException {
        Post originalPost = this.getPost(alterationDTO.getId());

        if(!user.equals(originalPost.getWriter())){
            throw new UnauthorizedException("The user: \""+ user +"\" does not have permission to alter post: " + originalPost.getId());
        }

        originalPost.setMessage(alterationDTO.getMessage());

        Post updatedObject = _postRepo.save(originalPost);
        return updatedObject;
    }

    public boolean addTestEnvironment(List<PostForAlterationDTO> newPosts, String user) {
        try{
            debug = true;
            List<Post> posts = getPostsByUser(user);
            if(posts != null && posts.size() > 0){
                _postRepo.deleteAll(posts);
                _postRepo.resetAutoIncrement();
            }

            int count = newPosts.size() + 1;
            for (PostForAlterationDTO post : emptyIfNull(newPosts)){
                post.setCreation_date(LocalDateTime.now().minusSeconds(count--));
                System.out.println(post);
                this.createPost(post, user);
            }

            debug = false;
            return true;
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
