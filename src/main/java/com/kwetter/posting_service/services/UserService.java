package com.kwetter.posting_service.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kwetter.posting_service.helpers.components.Producer;
import com.kwetter.posting_service.interfaces.IUserService;
import com.kwetter.posting_service.objects.data_transfer_objects.WriterDTO;
import com.kwetter.posting_service.objects.enums.MessageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {

    @Autowired
    Producer producer;

    public WriterDTO returnUser(String user_id){
        WriterDTO user = new WriterDTO();
        user.setId(user_id);

        ObjectMapper mapper = new ObjectMapper();
        Object result = producer.request(user, MessageType.SINGLE, "user.request");
        user = mapper.convertValue(result, WriterDTO.class);

        if(user == null){
            return null;
        }

        return user;
    }

    @Override
    public List<WriterDTO> returnUsers(List<String> user_ids) {
        ArrayList<Object> objectList = (ArrayList<Object>) producer.request(user_ids, MessageType.LIST, "user.request");
        List<WriterDTO> writers = new ArrayList<>();

        ObjectMapper mapper = new ObjectMapper();
        for(Object writer : objectList){
            WriterDTO writerDTO = mapper.convertValue(writer, WriterDTO.class);
            writers.add(writerDTO);
        }

        if(writers.isEmpty()){
            return new ArrayList<>();
        }

        return writers;
    }
}
