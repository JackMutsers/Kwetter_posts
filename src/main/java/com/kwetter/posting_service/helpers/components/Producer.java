package com.kwetter.posting_service.helpers.components;

import com.google.gson.Gson;
import com.kwetter.posting_service.objects.data_transfer_objects.RabbitDTO;
import com.kwetter.posting_service.objects.enums.MessageType;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    @Qualifier("template")
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${jsa.rabbitmq.exchange}")
    private String exchange;

    private final Gson gson = new Gson();

    public void send(Object item, String routingKey){
        amqpTemplate.convertAndSend(exchange, routingKey, item);
        System.out.println("Send msg = " + item);
    }

    public Object request(Object item, MessageType type, String routingKey){
        System.out.println("Request msg = " + item);

        RabbitDTO rabbitDTO = new RabbitDTO(item, type);

        String response = (String) amqpTemplate.convertSendAndReceive(exchange, routingKey, rabbitDTO);

        System.out.println("response msg = " + response);
        rabbitDTO = gson.fromJson(response, RabbitDTO.class);

        return rabbitDTO.getContent();
    }
}
