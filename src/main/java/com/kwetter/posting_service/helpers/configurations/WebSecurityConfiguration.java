package com.kwetter.posting_service.helpers.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.*;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${environment}")
    private String environment;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        System.out.println(environment);
        if(environment.equals("testing")){
            http.csrf().disable();
        }else{
            http.cors();
        }
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        List<String> allowedOriginPatterns = new ArrayList<>();
        allowedOriginPatterns.add("http://localhost:4200");
        allowedOriginPatterns.add("http://localhost:4201");
        allowedOriginPatterns.add("http://104.198.144.76");
        allowedOriginPatterns.add("http://104.198.144.76:4200");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().setAllowedOriginPatterns(allowedOriginPatterns));
        return source;
    }
}