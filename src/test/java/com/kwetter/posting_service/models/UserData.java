package com.kwetter.posting_service.models;

import lombok.Getter;

@Getter
public class UserData {
    private String localId;
    private String email;
    private String displayName;
    private String idToken;
}
