package com.kwetter.posting_service.helpers;

import com.google.gson.Gson;
import com.kwetter.posting_service.helpers.tools.Helper;
import com.kwetter.posting_service.models.UserData;
import com.sun.research.ws.wadl.HTTPMethods;
import okhttp3.*;

import java.io.IOException;

public class HttpService {
    private final String URL;

    private final Gson json_converter = new Gson();
    private final UserData userData;

    public UserData getUserData() {
        return userData;
    }

    public String getDisplayName() {
        return userData.getDisplayName();
    }

    public HttpService() throws IOException {
        this.URL = Helper.getGlobalVariable("connectionstring");

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\"email\":\"test@test.com\",\"password\":\"Muismat1\",\"returnSecureToken\":true}");
        String uri = "/auth/login";

        Response response = sendHttpMessage(uri, body, HTTPMethods.POST);

        assert response.body() != null;

        userData = json_converter.fromJson(response.body().string(), UserData.class);
    }

    // with body
    public Response sendHttpMessage(String uri, RequestBody body, HTTPMethods method) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder()
                .url("http://"+URL+":4000" + uri)
                .method(method.value(), body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", (this.userData == null || this.userData.getIdToken() == null ? "" : "Bearer " + this.userData.getIdToken()))
                .build();

        return client.newCall(request).execute();
    }

    // without body
    public Response sendHttpMessage(String uri, HTTPMethods method) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder()
                .url("http://"+URL+":4000" + uri)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", (this.userData == null || this.userData.getIdToken() == null ? "" : "Bearer " + this.userData.getIdToken()))
                .build();

        return client.newCall(request).execute();
    }

}
