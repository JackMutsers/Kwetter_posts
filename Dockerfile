#
# Build stage
#
#FROM maven:3.8.5-openjdk-17 AS build
#COPY src /home/app/src
#COPY pom.xml /home/app
#RUN mvn -f /home/app/pom.xml clean install -DskipTests
#
#FROM openjdk:11
#COPY --from=build /home/app/target/posting_service-0.0.1-SNAPSHOT.jar posting-service.jar
#CMD ["java","-jar","posting-service.jar"]


# syntax=docker/dockerfile:1

FROM maven:3.8.5-openjdk-17 AS base
WORKDIR /app
COPY .mvn/ ./.mvn
COPY mvnw ./
COPY pom.xml ./
RUN chmod +x ./mvnw
RUN chmod 777 ./pom.xml
COPY src ./src

FROM base AS test
CMD ["mvn","test","-Dconnectionstring=kwetter-gateway-test"]

FROM base AS build
RUN mvn clean install -DskipTests

FROM openjdk:11
COPY --from=build /app/target/posting_service-0.0.1-SNAPSHOT.jar posting-service.jar
CMD ["java","-jar","posting-service.jar"]