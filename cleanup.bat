@echo off
:: variables
set DOCKER_USERNAME=jmutsers
set CONTAINER=%1
set IMAGE=%DOCKER_USERNAME%/%CONTAINER%

:: get all old images
docker ps --format '{{.Image}}' | grep '%IMAGE%' > containers.txt
docker images --format '{{.Repository}}:{{.Tag}}' | grep '%IMAGE%' > images.txt
grep -v -F -f containers.txt images.txt > result.txt
del containers.txt
del images.txt

:: remove all old images
cat result.txt | xargs docker rmi
docker images --filter "dangling=true" | xargs docker rmi

del result.txt
